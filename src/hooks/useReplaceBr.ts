const replaceWithBr = function(str: string) {
    return str.replace(/\n/g, "<br />")
}

export {
    replaceWithBr
}