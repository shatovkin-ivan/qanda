import { RequestData } from '@/interfaces/UseRequestTypes'
const useRequest = async function(url: string, method: string, headers: any, body: object | null): Promise<RequestData> {
    try {
        const response = await fetch(url, {
            method,
            headers,
            body: body === null ? null : JSON.stringify(body),
        })
        const { status } = response
       
        if (response.ok) {
            const data = await response.json() 
            return {
                status,
                data
            }
        } else {
            return {
                status
            }
        }
    } catch(e) {
        throw new Error(`${e}`)
    }
}

export {
    useRequest
}

