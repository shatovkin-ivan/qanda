import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'

import Home from '@/views/Home.vue'
import RecommendedQuestionsVue from './components/recommended/RecommendedQuestions.vue'

const router = createRouter({
  history: process.env.NODE_ENV === 'production' ? createWebHistory() : createWebHashHistory(),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      children: [
        {
          path: ':url',
          name: 'recommended',
          component: RecommendedQuestionsVue
        }
      ]
    },
  ],
})

export default router
