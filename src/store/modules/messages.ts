interface StateTypes {
    isVisible: boolean,
    message: null | string,
    pageNotFound: boolean,
}
export default {
    state: {
        isVisible: false,
        message: null,
        pageNotFound: false,
    } as StateTypes,
    actions: {
        openModal(ctx: any, message: string) {
            ctx.commit('updateVisibility', true)
            ctx.commit('updateMessage', message)
        },
        closeModal(ctx: any) {
            ctx.commit('updateVisibility', false)
            ctx.commit('updateMessage', null)
        },
        pageNotFound(ctx: any) {
            ctx.commit('updatePageInfo', true)
        },
        pageFound(ctx: any) {
            ctx.commit('updatePageInfo', false)
        }
    },
    mutations: {
        updateVisibility(state: any, data: boolean) {
            state.isVisible = data
        },
        updateMessage(state: StateTypes, message: string) {
            state.message = message
        },
        updatePageInfo(state: StateTypes, data: boolean) {
            state.pageNotFound = data
        }
    },
    getters: {
        getModalVisibility(state: StateTypes) {
            return state.isVisible
        },
        getModalMessage(state: StateTypes) {
            return state.message
        },
        getPageInfo(state: StateTypes) {
            return state.pageNotFound
        }
    }
}