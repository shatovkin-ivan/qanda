interface StateTypes {
  authenticated: boolean | null,
}

export default {
  state: {
    authenticated: null,
  },
  actions: {
    isAuthenticated(ctx: any) {
      if (localStorage.getItem('isAuthenticated')) {
        ctx.commit('updateAuthenticated', true)
      } else {
        ctx.commit('updateAuthenticated', false)
      }
    },
    setAuthenticated(ctx: any) {
      localStorage.setItem('isAuthenticated', 'true')
      ctx.commit('updateAuthenticated', true)
    },
    removeAuthenticated(ctx: any) {
      localStorage.removeItem('isAuthenticated')
      ctx.commit('updateAuthenticated', false)
    },
  },
  mutations: {
    updateAuthenticated(state: StateTypes, data: boolean) {
      state.authenticated = data
    },
  },
  getters: {
    getAuthenticated(state: StateTypes) {
      return state.authenticated
    },
  },
}
