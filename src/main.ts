import MasonryWall from '@yeger/vue-masonry-wall'
import { vue3Debounce } from 'vue-debounce'
import { createApp } from 'vue'
import router from '@/router'
import store from '@/store'
import App from '@/App.vue'

const app = createApp(App)
app.use(router)
app.use(store)
app.use(MasonryWall)
app.directive('debounce', vue3Debounce({ lock: true }))
app.mount('#app')
