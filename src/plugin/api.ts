import { headersTypes } from '../interfaces/HeaderTypes'

function getHeaders() {
  return {
    'Ocp-Apim-Subscription-Key': `${process.env.VUE_APP_API_KEY}`,
    'Content-Type': 'application/json',
  }
}

function getHeadersWithAuthorization(token: string) {
  const headers: headersTypes = getHeaders()

  if (token) {
    headers['Authorization'] = `Bearer ${token}`
    return headers
  }

  return headers
}

export { getHeaders, getHeadersWithAuthorization }
