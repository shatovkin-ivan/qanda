export interface RecommendedQuestionsTypes {
    id: number,
    url: string,
    question: string,
    answer: string
}