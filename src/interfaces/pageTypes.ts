import { RecommendedQuestionsTypes } from './RecommendedQuestionsTypes'
export interface PageData {
  url: string
  question: string
  answer: string
  category: string
  likedByUser: boolean | null
  dislikedByUser: boolean | null
  recommendedQuestions: RecommendedQuestionsTypes[]
}
