export interface Account {
    homeAccountId:  string;
    environment:    string;
    tenantId:       string;
    username:       string;
    localAccountId: string;
    idTokenClaims:  IDTokenClaims;
}

export interface IDTokenClaims {
    exp:       number;
    nbf:       number;
    ver:       string;
    iss:       string;
    sub:       string;
    aud:       string;
    nonce:     string;
    iat:       number;
    auth_time: number;
    idp:       string;
    oid:       string;
    emails:    string[];
    tfp:       string;
    at_hash:   string;
}
