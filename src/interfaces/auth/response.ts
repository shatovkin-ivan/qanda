export interface Response {
    authority:          string;
    uniqueId:           string;
    tenantId:           string;
    scopes:             string[];
    account:            Account;
    idToken:            string;
    idTokenClaims:      IDTokenClaims;
    accessToken:        string;
    fromCache:          boolean;
    expiresOn:          string;
    correlationId:      string;
    requestId:          string;
    extExpiresOn:       string;
    familyId:           string;
    tokenType:          string;
    state:              string;
    cloudGraphHostName: string;
    msGraphHost:        string;
    fromNativeBroker:   boolean;
}

export interface Account {
    homeAccountId:  string;
    environment:    string;
    tenantId:       string;
    username:       string;
    localAccountId: string;
    idTokenClaims:  IDTokenClaims;
}

export interface IDTokenClaims {
    exp:       number;
    nbf:       number;
    ver:       string;
    iss:       string;
    sub:       string;
    aud:       string;
    nonce:     string;
    iat:       number;
    auth_time: number;
    idp:       string;
    oid:       string;
    emails:    string[];
    tfp:       string;
    at_hash:   string;
}
