export interface AnswerCategoriesTypes {
    name: string,
    id: string,
    isChoosen: boolean
}