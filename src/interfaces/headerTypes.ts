export interface headersTypes {
    'Ocp-Apim-Subscription-Key': string,
    'Content-Type': string,
    'Authorization'?: string
}