export interface tableGrabTypes {
    isDown: boolean,
    startX: number,
    scrollLeft: number,
}