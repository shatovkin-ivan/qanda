export interface RequestHeadersTypes {
    'Ocp-Apim-Subscription-Key': string,
    'Content-Type': string,
    'X-Continuation-Token'?: string,
    Authorization?: string,
}

export interface RequestData {
    status: number,
    data?: any,
}